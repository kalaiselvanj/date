# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 14:33:33 2022

@author: KJayavel
"""
import utils
import pandas as pd
import datetime
import re


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.ORDER_LINE_CHRG

    '''
    src_date='''select * from IN1544.Dim_Day_SQL_IN1544'''
    
    src_charge='''select * from IN1544.DIM_CHARGE_CATEG_PYTHON_IN1544'''
    
    src = pd.read_sql(src_query,conn)
    src1 = pd.read_sql(src_date,conn)
    src2 = pd.read_sql(src_charge,conn)
    
    
    df = pd.DataFrame()
    
    
    df['SALES_ORDER_NUM']=src['SALES_ORDER_NUM'].replace(('NULL','?'),(101,101)).fillna(101).astype('int64')
    df['SALES_ORDER_LINE_NUM']=src['SALES_ORDER_LINE_NUM'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['CHARGE_CATEG_ID']=src['CHARGE_CATEG_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['CHRG_CATEG_MAP_ID']=src['CHRG_CATEG_MAP_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['CHARGE_NM']=src['CHARGE_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CHARGE_AMT']=src['CHARGE_AMT'].replace(('NULL','?'),(0,0)).fillna(0).astype('float')
    df['CHRG_CRE_DT']=pd.to_datetime(src['CHRG_CRE_DT'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')),infer_datetime_format=True).fillna('01-01-1900 00:00:00')
    df['CHRG_CRE_DT_date']=pd.to_datetime(src['CHRG_CRE_DT'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')),infer_datetime_format=True).fillna('01-01-1900 00:00:00').dt.date
    df['CHRG_QTY']=src['CHRG_QTY'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['CHRG_CRE_DT_KEY']= main_src['Day_key'].fillna(-1).astype('int')
    df['charge_categ_key']=main_src_1['Charge_categ_Key'].fillna(-1).astype('int')


    main_src = pd.merge(df, src1, left_on = 'CHRG_CRE_DT_date', right_on = 'Date_id', how = 'left')
    main_src_1 = pd.merge(df, src2, left_on = 'CHARGE_CATEG_ID', right_on = 'CHARGE_CATEG_ID', how = 'left')
    
    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.STG_FACT_ORDER_LINE_CHRG_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?)'''
    collist=['SALES_ORDER_NUM', 'SALES_ORDER_LINE_NUM', 'TENANT_ORG_ID',
           'CHARGE_CATEG_ID', 'CHRG_CATEG_MAP_ID', 'CHARGE_NM', 'CHARGE_AMT',
           'CHRG_CRE_DT', 'CHRG_QTY', 'CHRG_CRE_DT_KEY',
           'charge_categ_key']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
  
    conn.commit()
    
    tgt_table='''insert into FACT_ORDER_LINE_CHRG_PYTHON_IN1544
    select SALES_ORDER_NUM, SALES_ORDER_LINE_NUM, TENANT_ORG_ID,
    CHARGE_CATEG_ID, CHRG_CATEG_MAP_ID, CHARGE_NM, CHARGE_AMT,
    CHRG_CRE_DT, CHRG_QTY, CHRG_CRE_DT_KEY,
    charge_categ_key from STG_FACT_ORDER_LINE_CHRG_PYTHON_IN1544'''
    
    cursor.execute(tgt_table)
    conn.commit()
    
    

    cursor.close()
    conn.close()
    
    
if __name__=='__main__':
    main()
    