# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 17:10:22 2022

@author: KJayavel
"""

import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.CUST_ADDR

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    src.columns
    
    df = pd.DataFrame()
    
    
    
    def datet(d):
        if(len(d)<=23):
            return pd.to_datetime(d,infer_datetime_format=True)
        elif(len(d)>23):
            if('AM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
            elif('PM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
        else:
            return(pd.to_datetime('01-01-1900'))
        
        

    df['ADDR_ID']=src['ADDR_ID'].replace(('NULL','?'),(101,101)).astype('float').fillna(101)
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['DATA_SRC_ID']=src['DATA_SRC_ID'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['VALID_TS']=pd.to_datetime(src['VALID_TS'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')),infer_datetime_format=True).fillna('01-01-1900 00:00:00')
    df['VALID_STS']=src['VALID_STS'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CITY']=src['CITY'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['MUNICIPALITY']=src['MUNICIPALITY'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['TOWN']=src['TOWN'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['VILLAGE']=src['VILLAGE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['COUNTY']=src['COUNTY'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['DISTRICT']=src['DISTRICT'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ZIP_CD']=src['ZIP_CD'].replace('NULL',101).fillna(101).astype('int')
    df['POSTAL_CD']=src['POSTAL_CD'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['ZIP_EXTN']=src['ZIP_EXTN'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['ADDR_TYPE']=src['ADDR_TYPE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['AREA']=src['AREA'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CNTRY_CD']=src['CNTRY_CD'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['STATE_PRVNCE_TYPE']=src['STATE_PRVNCE_TYPE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['OWNER_ID']=src['OWNER_ID'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['PARENT_ID']=src['PARENT_ID'].replace(('NULL','?'),(101,101)).astype('int').fillna(101)
    df['DELTD_YN']=src['DELTD_YN'].str.strip().astype('str')
    df['CRE_DT']=pd.to_datetime(src['CRE_DT']).replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900')
    df['CRE_USER']=src['CRE_USER'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['UPD_TS']=pd.to_datetime(src['UPD_TS'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')),infer_datetime_format=True).fillna('01-01-1900 00:00:00')
    df['UPD_USER']=src['UPD_USER'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')

    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.stg_dim_cust_address_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),null,?,?,?,?)'''
    collist=['ADDR_ID', 'TENANT_ORG_ID', 'DATA_SRC_ID','VALID_TS', 'VALID_STS',
           'CITY', 'MUNICIPALITY', 'TOWN', 'VILLAGE',
           'COUNTY', 'DISTRICT', 'ZIP_CD', 'POSTAL_CD', 'ZIP_EXTN', 'ADDR_TYPE',
           'AREA', 'CNTRY_CD', 'STATE_PRVNCE_TYPE', 'OWNER_ID', 'PARENT_ID',
           'DELTD_YN', 'CRE_DT', 'CRE_USER',
           'UPD_TS', 'UPD_USER']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
  
    conn.commit()


    insert_table='''insert into dim_cust_address_PYTHON_IN1544
    select stg.ADDR_ID, stg.TENANT_ORG_ID, stg.DATA_SRC_ID,stg.VALID_TS, stg.VALID_STS,
    stg.CITY, stg.MUNICIPALITY, stg.TOWN, stg.VILLAGE,
    stg.COUNTY, stg.DISTRICT, stg.ZIP_CD, stg.POSTAL_CD, stg.ZIP_EXTN, stg.ADDR_TYPE,
    stg.AREA, stg.CNTRY_CD, stg.STATE_PRVNCE_TYPE, stg.OWNER_ID, stg.PARENT_ID,
    stg.DELTD_YN, getdate(), null,stg.CRE_DT, stg.CRE_USER,
    stg.UPD_TS, stg.UPD_USER from stg_dim_cust_address_PYTHON_IN1544 stg
    left join dim_cust_address_PYTHON_IN1544 tgt
    on stg.ADDR_ID = tgt.ADDR_ID
    where tgt.ADDR_ID is null or 
    ((tgt.ADDR_ID is not null) and ((tgt.CITY <> stg.CITY) or (tgt.MUNICIPALITY <> stg.MUNICIPALITY) or (tgt.TOWN <> stg.TOWN) or 
    (tgt.VILLAGE <> stg.VILLAGE) or 
    (tgt.COUNTY <> stg.COUNTY) or 
    (tgt.DISTRICT <> stg.DISTRICT) or 
    (tgt.ZIP_CD <> stg.ZIP_CD) or (tgt.POSTAL_CD <> stg.POSTAL_CD) or (tgt.ZIP_EXTN <> stg.ZIP_EXTN) or (tgt.ADDR_TYPE <> stg.ADDR_TYPE) or 
    (tgt.AREA <> stg.AREA) or (tgt.CNTRY_CD <> stg.CNTRY_CD) or (tgt.STATE_PRVNCE_TYPE <> stg.STATE_PRVNCE_TYPE) or (tgt.OWNER_ID <> stg.OWNER_ID) or 
    (tgt.PARENT_ID <> stg.PARENT_ID)) and (tgt.End_Date is null))'''
    
    cursor.execute(insert_table)
    conn.commit()
            
    update_table='''update dim_cust_address_PYTHON_IN1544 set End_date = getdate()
     from dim_cust_address_PYTHON_IN1544 tgt
    left join stg_dim_cust_address_PYTHON_IN1544 stg
    on stg.ADDR_ID = tgt.ADDR_ID
    where ((tgt.ADDR_ID is not null) and ((tgt.CITY <> stg.CITY) or (tgt.MUNICIPALITY <> stg.MUNICIPALITY) or (tgt.TOWN <> stg.TOWN) or 
    (tgt.VILLAGE <> stg.VILLAGE) or 
    (tgt.COUNTY <> stg.COUNTY) or 
    (tgt.DISTRICT <> stg.DISTRICT) or 
    (tgt.ZIP_CD <> stg.ZIP_CD) or (tgt.POSTAL_CD <> stg.POSTAL_CD) or (tgt.ZIP_EXTN <> stg.ZIP_EXTN) or (tgt.ADDR_TYPE <> stg.ADDR_TYPE) or 
    (tgt.AREA <> stg.AREA) or (tgt.CNTRY_CD <> stg.CNTRY_CD) or (tgt.STATE_PRVNCE_TYPE <> stg.STATE_PRVNCE_TYPE) or (tgt.OWNER_ID <> stg.OWNER_ID) or 
    (tgt.PARENT_ID <> stg.PARENT_ID)) and (tgt.End_Date is null))'''
            
    cursor.execute(update_table)
    conn.commit()
    
    cursor.close()
    conn.close()
     
     
if __name__=='__main__':
    main()
        