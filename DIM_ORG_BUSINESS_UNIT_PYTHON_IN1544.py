# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 11:40:28 2022

@author: KJayavel
"""
import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.ORG_BUSINESS_UNIT

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    df = pd.DataFrame()
    

    df['ORG_ID']=src['ORG_ID'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['SRC_ORG_CD']=src['SRC_ORG_CD'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ORG_TYPE_ID']=src['ORG_TYPE_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['ORG_NM']=src['ORG_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['PARENT_ORG_ID']=src['PARENT_ORG_ID'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['PARENT_ORG_NM']=src['PARENT_ORG_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['WM_RDC_NUM']=src['WM_RDC_NUM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['WM_STORE_NUM']=src['WM_STORE_NUM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['WM_DSTRBTR_NO']=src['WM_DSTRBTR_NO'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['WH_IND']=src['WH_IND'].replace(('NULL','?'),(101,101)).fillna(101).astype('int64')
    df['DSV_IND']=src['DSV_IND'].replace(('NULL','?'),(101,101)).fillna(101).astype('int64')
    df['ACTV_IND']=src['ACTV_IND'].replace(('NULL','?'),(101,101)).fillna(101).astype('int64')
    df['EFF_BEGIN_DT']=pd.to_datetime(src['EFF_BEGIN_DT'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900'),infer_datetime_format=True).dt.date
    df['EFF_END_DT']=pd.to_datetime(src['EFF_END_DT'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900'),infer_datetime_format=True).dt.date
    df['CRE_DT']=pd.to_datetime(src['CRE_DT'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900'),infer_datetime_format=True).dt.date
    df['Is_Valid_Flag']='Y'
    df['UPD_TS']=pd.to_datetime(src['UPD_TS'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')).fillna('01-01-1900 00:00:00'),infer_datetime_format=True)


    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.STG_DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    collist=['ORG_ID', 'SRC_ORG_CD', 'ORG_TYPE_ID', 'ORG_NM', 'PARENT_ORG_ID',
           'PARENT_ORG_NM', 'WM_RDC_NUM', 'WM_STORE_NUM', 'WM_DSTRBTR_NO',
           'WH_IND', 'DSV_IND', 'ACTV_IND', 'EFF_BEGIN_DT', 'EFF_END_DT', 'CRE_DT',
           'Is_Valid_Flag', 'UPD_TS']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
  
    conn.commit()
    
    scd2_flag='''insert into DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544
    select stg.ORG_ID, stg.SRC_ORG_CD, stg.ORG_TYPE_ID, stg.ORG_NM, stg.PARENT_ORG_ID,
    stg.PARENT_ORG_NM, stg.WM_RDC_NUM, stg.WM_STORE_NUM, stg.WM_DSTRBTR_NO,
    stg.WH_IND, stg.DSV_IND, stg.ACTV_IND, stg.EFF_BEGIN_DT, stg.EFF_END_DT, stg.CRE_DT,
    stg.Is_Valid_Flag, stg.UPD_TS from STG_DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544 stg
    left join DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544 tgt
    on stg.ORG_ID = tgt.ORG_ID
    where tgt.ORG_ID is null or ((tgt.ORG_ID is not null) and ((tgt.ORG_NM <> stg.ORG_NM) or 
    (tgt.PARENT_ORG_ID <> stg.PARENT_ORG_ID) or (tgt.PARENT_ORG_NM <> stg.PARENT_ORG_NM) or 
    (tgt.WM_RDC_NUM <> stg.WM_RDC_NUM) or (tgt.WM_STORE_NUM <> stg.WM_STORE_NUM) or 
    (tgt.WM_DSTRBTR_NO <> stg.WM_DSTRBTR_NO)) and (tgt.Is_Valid_Flag = 'Y'))'''
    
    cursor.execute(scd2_flag)
    conn.commit()
    
    update_scd_2='''update DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544 set Is_Valid_Flag = 'N'
    from DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544 tgt
    left join  STG_DIM_ORG_BUSINESS_UNIT_PYTHON_IN1544 stg 
    on stg.ORG_ID = tgt.ORG_ID
    where ((tgt.ORG_ID is not null) and ((tgt.ORG_NM <> stg.ORG_NM) or 
    (tgt.PARENT_ORG_ID <> stg.PARENT_ORG_ID) or (tgt.PARENT_ORG_NM <> stg.PARENT_ORG_NM) or 
    (tgt.WM_RDC_NUM <> stg.WM_RDC_NUM) or (tgt.WM_STORE_NUM <> stg.WM_STORE_NUM) or 
    (tgt.WM_DSTRBTR_NO <> stg.WM_DSTRBTR_NO)) and (tgt.Is_Valid_Flag = 'Y'))'''
    
    cursor.execute(update_scd_2)
    conn.commit()
    
    cursor.close()
    conn.close()
    
    
if __name__=='__main__':
    main()