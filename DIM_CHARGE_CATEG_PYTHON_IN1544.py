# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 17:58:48 2022

@author: KJayavel
"""

import utils
import pandas as pd
from datetime import datetime
#import logging

logger=utils.setlogger(logfile='DIM_CHARGE_CATEG_PY.log')

def main():
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from [BCMPWMT].CHARGE_CATEG_LKP

    '''
    
    src_charge_categdf=pd.read_sql(src_query,conn)
   
    
    df = pd.DataFrame()
    

    df['CHARGE_CATEG_ID']=src_charge_categdf['CHARGE_CATEG_ID'].str.strip().astype('int')
    df['TENANT_ORG_ID']=src_charge_categdf['TENANT_ORG_ID'].str.strip().astype('int')
    df['CHARGE_CATEG']=src_charge_categdf['CHARGE_CATEG'].apply(lambda x: x.strip() if len(x)>5 else str.upper(x.strip()))
    df['CHARGE_CATEG_DESC']=src_charge_categdf['CHARGE_CATEG_DESC'].str.strip()
    df['TAX_IND']=src_charge_categdf['TAX_IND'].str.strip().astype('int')
    
    
    cleaned_df=utils.nullhandler(df)
    print(cleaned_df)
    

    insertstmt=''
    for index,row in cleaned_df.iterrows():
        
        insertstmt+=f'''insert into IN1544.STG_DIM_CHARGE_CATEG_PYTHON_IN1544 
        values ({row['CHARGE_CATEG_ID']},{row['TENANT_ORG_ID']},'{row['CHARGE_CATEG']}','{row['CHARGE_CATEG_DESC']}', {row['TAX_IND']},1)
        '''
        
       
        print(insertstmt)
   
    cursor.execute(insertstmt)
    
 
    conn.commit()
    
    

    scd=""" insert into DIM_CHARGE_CATEG_PYTHON_IN1544
select stg.CHARGE_CATEG_ID,stg.TENANT_ORG_ID,stg.CHARGE_CATEG,stg.CHARGE_CATEG_DESC,
stg.TAX_IND, 
case
when tgt.CHARGE_CATEG_ID is null then 1
else 1+(select max(t.version) from  DIM_CHARGE_CATEG_PYTHON_IN1544 t join
 STG_DIM_CHARGE_CATEG_PYTHON_IN1544 s  on 
s.CHARGE_CATEG_ID = t.CHARGE_CATEG_ID
where t.CHARGE_CATEG <> s.CHARGE_CATEG)
end as version
from STG_DIM_CHARGE_CATEG_PYTHON_IN1544 stg
left join DIM_CHARGE_CATEG_PYTHON_IN1544 tgt
on stg.CHARGE_CATEG_ID = tgt.CHARGE_CATEG_ID
left join 
(select CHARGE_CATEG_ID, max(version) as max_version from DIM_CHARGE_CATEG_PYTHON_IN1544 group by CHARGE_CATEG_ID) a
on a.CHARGE_CATEG_ID = tgt.CHARGE_CATEG_ID
where tgt.CHARGE_CATEG_ID is null or ((tgt.CHARGE_CATEG_ID is not null) and (stg.CHARGE_CATEG <> tgt.CHARGE_CATEG)
and (tgt.version = a.max_version))"""

    cursor.execute(scd)
    conn.commit()
if __name__=='__main__':
    main()
    
