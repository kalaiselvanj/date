# -*- coding: utf-8 -*-
"""
Created on Sat Sep 10 15:09:02 2022

@author: KJayavel
"""
import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.CUST

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    df = pd.DataFrame()
    
    df = src['CUST_ID']
    
    def suff(name):
        return (name[0:1]+name[1:2].upper()+name[2:len(name)])
    
    def datet(d):
        if(len(d)<=23):
            return pd.to_datetime(d,infer_datetime_format=True)
        elif(len(d)>23):
            if('AM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
            elif('PM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
        else:
            return(pd.to_datetime('01-01-1900'))

    
 
    
    df['CUST_ID']=src['CUST_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['CUST_TYPE_ID']=src['CUST_TYPE_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['NICKNAME']=src['NICKNAME'].replace('NULL','N/A').str.title().fillna('N/A')
    df['SALUTE']=src['SALUTE'].replace('NULL','N/A').str.slice(0,4).fillna('N/A')
    df['MIDDLE_NM']=src['MIDDLE_NM'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['CUST_TITLE']=src['CUST_TITLE'].replace(('NULL', '#'),('N/A', '')).str.strip().fillna('N/A')
    df['SUFFIX_1']=src['SUFFIX'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['SUFFIX']=df.apply(lambda x: suff(x['SUFFIX_1']),axis=1).replace('NULL','N/A').str.strip().fillna('N/A')
    df['WM_EMPLOYEE_ID']=src['WM_EMPLOYEE_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['CRE_DT']=pd.to_datetime(src['CRE_DT']).replace('NULL','01-01-1900').fillna('01-01-1900')
    df['CRE_USER']=src['CRE_USER'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['UPD_TS_1']=src['UPD_TS'].replace('NULL','01-01-1900 00:00:00').fillna('01-01-1900 00:00:00')
    df['UPD_TS']=df.apply(lambda x: datet(x['UPD_TS_1']),axis=1)
    df['UPD_USER']=src['UPD_USER'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['SIGNUP_TS']=pd.to_datetime(src['SIGNUP_TS'].replace('NULL','01-01-1900 00:00:00'),infer_datetime_format=True).fillna('01-01-1900 00:00:00')
    df['REALM_ID']=src['REALM_ID'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['VALID_CUST_IND']=src['VALID_CUST_IND'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['DELTD_YN']=src['DELTD_YN'].str.strip().replace('NULL','N/A').fillna('N/A')
    
    insert=''
    for index,row in df.iterrows():
      insert+=f'''insert into IN1544.stg_dim_cust_PYTHON_IN1544 
      values ({row['CUST_ID']},{row['TENANT_ORG_ID']},{row['CUST_TYPE_ID']},'{row['NICKNAME']}', '{row['SALUTE']}',
              '{row['MIDDLE_NM']}','{row['CUST_TITLE']}','{row['SUFFIX']}',{row['WM_EMPLOYEE_ID']},
              '{row['CRE_DT']}','{row['CRE_USER']}','{row['UPD_TS']}','{row['UPD_USER']}',getdate(),null,'{row['SIGNUP_TS']}',
              '{row['REALM_ID']}','{row['VALID_CUST_IND']}','{row['DELTD_YN']}')
      '''
      
            
      print(insert)
        
    cursor.execute(insert)
    
 
    conn.commit()
    

    
    
    
    
if __name__=='__main__':
    main()
    