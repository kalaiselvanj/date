# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 17:55:55 2022

@author: KJayavel
"""
import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.RPT_HRCHY

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    df = pd.DataFrame()

    
    df['RPT_HRCHY_ID']=src['RPT_HRCHY_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['SRC_RPT_HRCHY_ID']=src['SRC_RPT_HRCHY_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace(('NULL','?'),('N/A','N/A')).fillna('N/A').astype('int').astype('str')
    df['RPT_HRCHY_PATH']=src['RPT_HRCHY_PATH'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['DIV_ID']=src['DIV_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['DIV_NM']=src['DIV_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['SUPER_DEPT_ID']=src['SUPER_DEPT_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['SUPER_DEPT_NM']=src['SUPER_DEPT_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['DEPT_ID']=src['DEPT_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['DEPT_NM']=src['DEPT_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CATEG_NM']=src['CATEG_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['SUB_CATEG_ID']=src['SUB_CATEG_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['SUB_CATEG_NM']=src['SUB_CATEG_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ITEM_CATEG_GROUPING_ID']=src['ITEM_CATEG_GROUPING_ID'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['SRC_CRE_TS']=src['SRC_CRE_TS'].apply(lambda x: x[0:len(x)-7]).replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')).fillna('01-01-1900 00:00:00')
    df['SRC_MODFD_TS']=src['SRC_MODFD_TS'].apply(lambda x: x[0:len(x)-7]).replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')).fillna('01-01-1900 00:00:00')
    df['SRC_HRCHY_MODFD_TS']=pd.to_datetime(src['SRC_HRCHY_MODFD_TS'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')).fillna('01-01-1900 00:00:00'),infer_datetime_format=True)
    df['CATEG_MGR_NM']=src['CATEG_MGR_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['BUYER_NM']=src['BUYER_NM'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['EFF_BEGIN_DT']=pd.to_datetime(src['EFF_BEGIN_DT'].apply(pd.to_datetime, errors='coerce').replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900')).dt.date
    df['EFF_END_DT']=pd.to_datetime(src['EFF_END_DT'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900')).dt.date
    df['RPT_HRCHY_ID_PATH']=src['RPT_HRCHY_ID_PATH'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CATEG_ID']=src['CATEG_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['CONSUMABLE_IND']=src['CONSUMABLE_IND'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CURR_IND']=src['CURR_IND'].replace(('NULL','?'),(101,101)).fillna(101).astype('float')
    df['CRE_DT']=pd.to_datetime(src['CRE_DT'].apply(pd.to_datetime, errors='coerce').replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900')).dt.date
    df['CRE_USER']=src['CRE_USER'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['UPD_TS']=pd.to_datetime(src['UPD_TS'].replace(('NULL','?'),('01-01-1900 00:00:00','01-01-1900 00:00:00')).fillna('01-01-1900 00:00:00'),infer_datetime_format=True)
    df['UPD_USER']=src['UPD_USER'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')

    
    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.stg_dim_rpt_hrchy_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    collist=['RPT_HRCHY_ID', 'SRC_RPT_HRCHY_ID', 'TENANT_ORG_ID', 'RPT_HRCHY_PATH',
           'DIV_ID', 'DIV_NM', 'SUPER_DEPT_ID', 'SUPER_DEPT_NM', 'DEPT_ID',
           'DEPT_NM', 'CATEG_NM', 'SUB_CATEG_ID', 'SUB_CATEG_NM',
           'ITEM_CATEG_GROUPING_ID', 'SRC_CRE_TS', 'SRC_MODFD_TS',
           'SRC_HRCHY_MODFD_TS', 'CATEG_MGR_NM', 'BUYER_NM', 'EFF_BEGIN_DT',
           'EFF_END_DT', 'RPT_HRCHY_ID_PATH', 'CATEG_ID', 'CONSUMABLE_IND',
           'CURR_IND', 'CRE_DT', 'CRE_USER', 'UPD_TS', 'UPD_USER']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
    
    conn.commit()
    
    tgt_table = '''insert into dim_rpt_hrchy_PYTHON_IN1544
    select 
    RPT_HRCHY_ID, SRC_RPT_HRCHY_ID, TENANT_ORG_ID, RPT_HRCHY_PATH,
    DIV_ID, DIV_NM, SUPER_DEPT_ID, SUPER_DEPT_NM, DEPT_ID,
    DEPT_NM, CATEG_NM, SUB_CATEG_ID, SUB_CATEG_NM,
    ITEM_CATEG_GROUPING_ID, SRC_CRE_TS, SRC_MODFD_TS,
    SRC_HRCHY_MODFD_TS, CATEG_MGR_NM, BUYER_NM, EFF_BEGIN_DT,
    EFF_END_DT, RPT_HRCHY_ID_PATH, CATEG_ID, CONSUMABLE_IND,
    CURR_IND, CRE_DT, CRE_USER, UPD_TS, UPD_USER
    from stg_dim_rpt_hrchy_PYTHON_IN1544'''
    
    cursor.execute(tgt_table)
    conn.commit()
    
    cursor.close()
    conn.close()
    
    
if __name__=='__main__':
    main()