# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 14:52:40 2022

@author: KJayavel
"""
import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.CUST_ACCT

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    df = pd.DataFrame()
    
    def email(a):
        if('@' in a):
            return a.str.strip()
        else:
            return 'N/A'
    
    def datet(d):
        if(len(d)<=23):
            return pd.to_datetime(d,infer_datetime_format=True)
        elif(len(d)>23):
            if('AM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
            elif('PM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
        else:
            return(pd.to_datetime('01-01-1900'))



    df['ACCT_ID']=src['ACCT_ID'].replace('NULL',101).str.strip().astype('long').fillna(101)
    df['CUST_ID']=src['CUST_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['ACCT_STS_ID']=src['ACCT_STS_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['ACCT_TYPE_ID']=src['ACCT_TYPE_ID'].replace('NULL',101).str.strip().astype('int').fillna(101)
    df['EMAIL_1']=src['EMAIL'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['EMAIL']=df.apply(lambda x: email(x['EMAIL_1']),axis=1)
    df['VALID_CUST_IND']=src['VALID_CUST_IND'].replace('NULL',101).astype('int').fillna(101)  
    df['CRE_DT']=pd.to_datetime(src['CRE_DT']).replace('NULL','01-01-1900').fillna('01-01-1900')
    df['CRE_USER']=src['CRE_USER'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['UPD_TS']=pd.to_datetime(src['UPD_TS'].replace('NULL','01-01-1900 00:00:00').str.strip().fillna('01-01-1900 00:00:00'),infer_datetime_format=True)
    df['UPD_USER']=src['UPD_USER'].replace('NULL','N/A').str.strip().fillna('N/A')
    df['DELTD_YN']=src['DELTD_YN'].str.strip().astype('str')

    insert=''
    for index,row in df.iterrows():
      insert+=f'''insert into IN1544.stg_dim_CUST_ACCT_PYTHON_IN1544 
      values ({row['ACCT_ID']},{row['CUST_ID']},{row['TENANT_ORG_ID']},{row['ACCT_STS_ID']}, {row['ACCT_TYPE_ID']},
              '{row['EMAIL']}',{row['VALID_CUST_IND']},'{row['CRE_DT']}','{row['CRE_USER']}',
              '{row['UPD_TS']}','{row['UPD_USER']}',getdate(),null,'{row['DELTD_YN']}')
      '''
      
            
      print(insert)
      
      cursor.execute(insert)
      
   
      conn.commit()
      
      
    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.stg_dim_CUST_ACCT_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?,getdate(),null,?)'''
    collist=['ACCT_ID', 'CUST_ID', 'TENANT_ORG_ID',
           'ACCT_STS_ID', 'ACCT_TYPE_ID', 'EMAIL', 'VALID_CUST_IND',
           'CRE_DT', 'CRE_USER', 'UPD_TS', 'UPD_USER','DELTD_YN']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
  
    conn.commit()
        
  

        

        
        
        
        
    if __name__=='__main__':
        main()
        