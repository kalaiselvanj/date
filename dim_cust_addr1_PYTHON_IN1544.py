# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 19:04:03 2022

@author: KJayavel
"""

import utils
import pandas as pd
import datetime


def main():
    
    conn,cursor= utils.create_conn()
    
    
    src_query='''
    select * from BCMPWMT.CUST_ADDR1

    '''
    
    src = pd.read_sql(src_query,conn)
    
    
    src.columns
    
    df = pd.DataFrame()
    
    
    
    def datet(d):
        if(len(d)<=23):
            return pd.to_datetime(d,infer_datetime_format=True)
        elif(len(d)>23):
            if('AM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
            elif('PM' in d):
                return pd.to_datetime(d,infer_datetime_format=True)
        else:
            return(pd.to_datetime('01-01-1900'))
        
        
    df['ADDR_ID']=src['ADDR_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int64')
    df['TENANT_ORG_ID']=src['TENANT_ORG_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['DATA_SRC_ID']=src['DATA_SRC_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['VALID_TS']=pd.to_datetime(src['VALID_TS'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900')).dt.strftime('%d-%b-%Y')
    df['VALID_STS']=src['VALID_STS'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['CITY']=src['CITY'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['MUNICIPALITY']=src['MUNICIPALITY'].replace(('NULL','?'),('N/A','N/A')).str.slice(0,8).fillna('N/A')
    df['TOWN']=src['TOWN'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['VILLAGE']=src['VILLAGE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['COUNTY']=src['COUNTY'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['DISTRICT']=src['DISTRICT'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ZIP_CD']=src['ZIP_CD'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['POSTAL_CD']=src['POSTAL_CD'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ZIP_EXTN']=src['ZIP_EXTN'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['ADDR_TYPE']=src['ADDR_TYPE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['AREA']=src['AREA'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CNTRY_CD']=src['CNTRY_CD'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['STATE_PRVNCE_TYPE']=src['STATE_PRVNCE_TYPE'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['OWNER_ID']=src['OWNER_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['PARENT_ID']=src['PARENT_ID'].replace(('NULL','?'),(101,101)).fillna(101).astype('int')
    df['DELTD_YN']=src['DELTD_YN'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')
    df['CRE_DT']=pd.to_datetime(src['CRE_DT'].replace(('NULL','?'),('01-01-1900','01-01-1900')).fillna('01-01-1900'))
    df['CRE_USER']=src['CRE_USER'].replace(('NULL','?'),('N/A','N/A')).str.strip().fillna('N/A')

    cursor.fast_executemany = True
    insert_to_tmp_tbl_stmt='''insert into IN1544.stg_dim_cust_addr1_PYTHON_IN1544
     values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
    collist=['ADDR_ID', 'TENANT_ORG_ID', 'DATA_SRC_ID', 'VALID_TS', 'VALID_STS',
           'CITY', 'MUNICIPALITY', 'TOWN', 'VILLAGE', 'COUNTY', 'DISTRICT',
           'ZIP_CD', 'POSTAL_CD', 'ZIP_EXTN', 'ADDR_TYPE', 'AREA', 'CNTRY_CD',
           'STATE_PRVNCE_TYPE', 'OWNER_ID', 'PARENT_ID', 'DELTD_YN', 'CRE_DT',
           'CRE_USER']
    cursor.executemany(insert_to_tmp_tbl_stmt, df[collist].values.tolist())
  
    conn.commit()
    
    insert_tgt_table ='''insert into dim_cust_addr1_PYTHON_IN1544
    select stg.ADDR_ID, stg.TENANT_ORG_ID, stg.DATA_SRC_ID, stg.VALID_TS, stg.VALID_STS,
    stg.CITY, stg.MUNICIPALITY, stg.TOWN, stg.VILLAGE, stg.COUNTY, stg.DISTRICT,
    stg.ZIP_CD, stg.POSTAL_CD, stg.ZIP_EXTN, stg.ADDR_TYPE, stg.AREA, stg.CNTRY_CD,
    stg.STATE_PRVNCE_TYPE, stg.OWNER_ID, stg.PARENT_ID, stg.DELTD_YN, stg.CRE_DT,
    stg.CRE_USER from stg_dim_cust_addr1_PYTHON_IN1544 stg
    left join dim_cust_addr1_PYTHON_IN1544 tgt
    on stg.ADDR_ID = tgt.ADDR_ID
    where tgt.ADDR_ID is null'''
       
    cursor.execute(insert_tgt_table)
    
    conn.commit()
    
    update_tgt_table ='''update dim_cust_addr1_PYTHON_IN1544 set city = stg.city, MUNICIPALITY=stg.MUNICIPALITY,
    TOWN = stg.TOWN, VILLAGE = stg.VILLAGE, COUNTY = stg.COUNTY, DISTRICT = stg.DISTRICT
    from stg_dim_cust_addr1_PYTHON_IN1544 stg join
    dim_cust_addr1_PYTHON_IN1544 tgt
    on stg.ADDR_ID = tgt.ADDR_ID
    where ((stg.city <> tgt.city) or (stg.MUNICIPALITY <> tgt.MUNICIPALITY) or (stg.TOWN <> tgt.TOWN) or
     (stg.VILLAGE <> tgt.VILLAGE) or (stg.COUNTY <> tgt.COUNTY) or (stg.DISTRICT <> tgt.DISTRICT))'''
       
    cursor.execute(update_tgt_table)
    
    conn.commit()
    
    cursor.close()
    conn.close()
        
if __name__=='__main__':
    main()